# PruebaJSF

Adaptación de un proyecto JSF con Bootstrap 4 existente a [Liferay Faces](https://portal.liferay.dev/docs/7-0/tutorials/-/knowledge_base/t/jsf-portlets-with-liferay-faces) y bootstrap 3. 

+ Almacenamiento de datos en memoria mediante un ManagedBean de aplicación.
+ Prueba de llamada a un servicio Liferay local.


Falta gestión de mensajes de aplicación.