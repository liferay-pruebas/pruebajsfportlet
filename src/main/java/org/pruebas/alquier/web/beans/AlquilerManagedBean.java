package org.pruebas.alquier.web.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.pruebas.alquiler.modelo.DVDItem;
import org.pruebas.alquiler.serviceTracker.GuestbookLocalServiceTracker;

import com.liferay.docs.guestbook.model.Guestbook;
import com.liferay.docs.guestbook.service.GuestbookLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;

/**
 *
 * @author david
 */
@ManagedBean(name = "alquiler")
@SessionScoped
public class AlquilerManagedBean implements Serializable {

    @ManagedProperty(value = "#{app}")
    private AppManagedBean appManagedBean;
    private DVDItem dvd = new DVDItem();

    private boolean modoNuevo = false;
    private int idViejo;
    
    private GuestbookLocalServiceTracker  guestbookServiceTracker;

    /**
     * Creates a new instance of AlquilerManagedBean
     */
    public AlquilerManagedBean() {

    }
    
    @PostConstruct
    public void postConstruct() {
        Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        guestbookServiceTracker = new GuestbookLocalServiceTracker(bundleContext);
        guestbookServiceTracker.open();
        GuestbookLocalService guestbookService = guestbookServiceTracker.getService();
        System.out.println("Servicio local: " + guestbookService);
        if(guestbookService == null){
        	System.out.println("No se ha encontrado el servicio+++++++++++++++++++++++++++++++++++++++++");
        }
    }
    
    @PreDestroy
    public void preDestroy() {
    	guestbookServiceTracker.close();
    }

    public void setAppManagedBean(AppManagedBean appManagedBean) {
        this.appManagedBean = appManagedBean;
    }

    public Collection<DVDItem> getLista() {
        return appManagedBean.getDvds().values();
    }

    public DVDItem getDvd() {
        return dvd;
    }

    public void setDvd(DVDItem dvd) {
        this.dvd = dvd;
    }

    public boolean isModoNuevo() {
        return modoNuevo;
    }

    public String alquilar(int id) {

        DVDItem d = this.buscar(id);
        if (d != null) {
            if (d.isAlquilado()) {
                //Error
            } else {
                d.setAlquilado(true);
            }
        }//else error

        return null;
    }

    public String devolver(int id) {
        DVDItem d = this.buscar(id);

        if (d != null) {
            if (d.isAlquilado()) {
                d.setAlquilado(false);
            } else {
                //Error
            }
        }//else error

        return null;
    }

    private DVDItem buscar(int id) {
        return appManagedBean.getDvds().get(id);
    }

    //para llamar desde boton
    public String altaDVD() {
        //preparo los datos del bean para hacer un alta
        this.modoNuevo = true;
        this.dvd = new DVDItem();
        return "crearDVD";
    }

    //para llamar desde boton
    public String modificarDVD(int id) {
        this.modoNuevo = false;
        this.dvd = this.buscar(id);
        this.idViejo = this.dvd.getId();
        return "crearDVD";//Se usa la misma p�gina para las acciones de modificar y crear
    }

    //se le llama desde el formulario de guardado/modificaci�n
    public String crearDVD() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (modoNuevo) {
            if (this.buscar(dvd.getId()) == null) {
                appManagedBean.getDvds().put(dvd.getId(), dvd);
                dvd = new DVDItem();
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DVD a�adido", "Se ha a�adido el nuevo DVD con �xito"));//id del componente al que se asociar�a y mensaje
            }else{
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Id repetido", "El id proporcionado ya existe"));//id del componente al que se asociar�a y mensaje
                return "crearDVD";
            }
        } else {
            if(this.buscar(dvd.getId()) == null){
                appManagedBean.getDvds().remove(this.idViejo);
                appManagedBean.getDvds().put(dvd.getId(), dvd);
                dvd = new DVDItem();
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DVD modificado", "Se ha modificado el DVD con �xito"));
            }else if(dvd.getId() == idViejo){
                appManagedBean.getDvds().put(idViejo, dvd);
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DVD modificado", "Se ha modificado el DVD con �xito"));
            }
            else{
                fc.addMessage("ERROR", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Id repetido", "El id proporcionado ya existe"));
                return "crearDVD";
            }
        }

        return "alquiler";
    }
    
    /**
     * Prueba llamada local sercice
     * @return
     */
    public int getNumGuestbooks(){
    	GuestbookLocalService guestbookService = guestbookServiceTracker.getService();
        System.out.println("Servicio local: " + guestbookService);
        if(guestbookService == null){
        	System.out.println("No se ha encontrado el servicio+++++++++++++++++++++++++++++++++++++++++");
        }
    	return guestbookService.getGuestbooksCount();
    }
    
    public List<Guestbook> getListGuestbooks(){
    	ServiceContext sctx;
		try {
			sctx = ServiceContextFactory.getInstance((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
			GuestbookLocalService guestbookService = guestbookServiceTracker.getService();
	        System.out.println("Servicio local: " + guestbookService);
	        if(guestbookService == null){
	        	System.out.println("No se ha encontrado el servicio+++++++++++++++++++++++++++++++++++++++++");
	        }
	    	return guestbookService.getGuestbooks(sctx.getScopeGroupId());
	    	
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
}
