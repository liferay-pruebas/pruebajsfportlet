package org.pruebas.alquier.web.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.pruebas.alquiler.modelo.DVDItem;

import javax.faces.model.SelectItem;

/**
 *
 * @author david
 */
@ManagedBean(name = "app")
@ApplicationScoped
public class AppManagedBean {
    private List<SelectItem> categorias;
    private String genero = new String();
    
    private LinkedHashMap<Integer, DVDItem> dvds;
    
    public AppManagedBean() {
        dvds = new LinkedHashMap();
        dvds.put(1, new DVDItem(1, "Las cr�nicas de Narnia : el l...", "Fantas�a", false));
        dvds.put(2, new DVDItem(2, "Star Trek", "Ciencia ficci�n", true));
        dvds.put(3, new DVDItem(3, "Bla bla bla", "Suspense", false));
        dvds.put(4, new DVDItem(4, "Avengers:...", "Ficci�n", false));
        
        this.categorias = new ArrayList();
        this.categorias.add(new SelectItem("Ficci�n"));
        this.categorias.add(new SelectItem("Suspense"));
        this.categorias.add(new SelectItem("Ciencia ficci�n"));
        this.categorias.add(new SelectItem("Animaci�n"));
        this.categorias.add(new SelectItem("Hist�rico"));
        this.categorias.add(new SelectItem("Terror"));
    }

    public List<SelectItem> getCategorias() {
        return categorias;
    }

    public LinkedHashMap<Integer, DVDItem> getDvds() {
        return dvds;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String anadirGenero(){
        FacesContext fc = FacesContext.getCurrentInstance();

        this.categorias.add(new SelectItem(this.genero));
        this.genero = new String();
        fc.addMessage("INFO", new FacesMessage("Se ha a�adido el nuevo g�nero"));
        return "genero";
    }
    
}